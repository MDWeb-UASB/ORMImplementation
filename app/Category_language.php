<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_language extends Model
{
    //
	protected $table = 'category_language';
	
	public function categories()
	{
		return $this->belongsTo('App/categories');
	}
	public function languages()
	{
		return $this->belongsTo('App/languages');
	}
}
