<?php

use Illuminate\Database\Seeder;

use App\Posts;
use App\Photos;
use Faker\Generator;


class PhotosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		 // thanks to Carlos Montellano on the tip to get images
        $faker = Faker\Factory::create();
        $posts = Posts::get();// obtener todos los posts
        factory(Photos::class,30)->create()->each(function($photo) use ($posts,$faker){
        $posts_random = $posts->random( rand(0,6) );
        for ($i=0;$i<count($posts_random);$i++)
        {
            $registro[]=array(
                    'posts_id'=>$posts_random[$i]['id'],
                    'use'=>$faker->word,
                    'order'=>  $i+1,
                    );
        }
         if (count($posts_random)>1) 
        {$photo->posts()->sync($registro);}

        });

    }
}
