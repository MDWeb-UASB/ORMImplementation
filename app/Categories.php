<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //
	protected $table = 'categories';
	
	public function category_languages()
	{
		return $this->belongsToMany(Languages::class, 'category_languages', 'categories_id', 'languages_id')
			->withPivot('label', 'slug','description')
    	    ->withTimestamps();
	}
	
}
