<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Languages;
use App\Publishes;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(User::class, 50)->create();
        $this->call(LanguagesTableSeeder::class);
		$this->call(CategoriesSeeder::class);
		$this->call(Category_languageSeeder::class);
		$this->call(PublishesSeeder::class);
		$this->call(PostsSeeder::class);
		$this->call(CommentsSeeder::class);		
		$this->call(PhotosSeeder::class);	

    }
}
