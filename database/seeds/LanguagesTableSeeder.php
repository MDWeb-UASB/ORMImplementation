<?php

use Illuminate\Database\Seeder;
use App\Languages;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'iso6391' => 'es',
			'label' => 'es'
        ]);
		DB::table('languages')->insert([
            'iso6391' => 'en',
			'label' => 'en'
        ]);

    }
}
