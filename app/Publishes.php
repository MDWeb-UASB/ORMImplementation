<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publishes extends Model
{
    //
	protected $table = 'publishes';
	
	protected $fillable = [
        'slug','label', 'is_publish',
    ];

	
	public function posts()
    {
        return $this->hasMany('App\Posts');
    }
}
