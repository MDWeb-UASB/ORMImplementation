<?php

use Illuminate\Database\Seeder;
use App\Languages;
use App\Categories;

class Category_languageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();
        $languages = Languages::get();
        factory(Categories::class,10)->create()->each(function($category) use ($languages, $faker){
			foreach ($languages as $language) 
            {
            
            $category_language[]=array(
                    'languages_id'=>$language->id,
                    'label'=>$faker->sentence(2),
                    'slug'=>$faker->slug(3),
                    'description'=>$faker->text(rand(10, 15)),
                    );
            } 

            $category->category_languages()->sync($category_language);
        });

    }
}
