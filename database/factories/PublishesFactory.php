<?php

use Faker\Generator as Faker;

$factory->define(App\Publishes::class, function (Faker $faker) {
    return [
       'slug' =>$faker->slug,
       'label' =>$faker->word,
       'is_publish' =>$faker->boolean($chanceOfGettingTrue=60),
    ];

});
