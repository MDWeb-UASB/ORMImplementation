<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    //
	public function published()
    {
        return $this->belongsTo('App\Publishes');
    }
}
