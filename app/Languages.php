<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    //
	protected $table = 'languages';

	public function category_languages()
	{
		return $this->hasMany('App/category_languages');
	}	
}
