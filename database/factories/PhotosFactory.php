<?php

use Faker\Generator as Faker;

$factory->define(App\Photos::class, function (Faker $faker) {
     $types = [
        "jpg",
    ];

        return [
        //
        'filename' =>$faker->unique()->image('public/images',150,150, null, false), 
        'type'=> $types [array_rand($types,1)],
    ];
});

