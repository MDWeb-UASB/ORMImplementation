<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->unsigned();
			$table->integer('categories_id')->unsigned();
			$table->integer('publishes_id')->unsigned();
            $table->dateTime('publish_at')->nullable();
            $table->timestamps();
			$table->timestamp('deleted_at')->nullable();
			
            $table->foreign('author_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
			
			// Added this to allow having posts by category
			$table->foreign('categories_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
