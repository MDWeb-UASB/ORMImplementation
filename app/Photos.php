<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{
     public function posts()
    {
        return $this->belongsToMany(Posts::class,'photo_post','photos_id','posts_id')
            ->withPivot('use', 'order')
    	    ->withTimestamps();
        
    }

}
