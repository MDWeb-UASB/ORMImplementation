<?php

use Illuminate\Database\Seeder;
use App\Publishes;

class PublishesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Publishes::class,15)->create();
    }
}
